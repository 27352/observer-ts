import {IObserver} from './IObserver';

export interface IObservable<T> {
    getValue(): T;
}
