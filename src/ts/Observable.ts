import {IObservable} from './IObservable';
import {IObserver} from './IObserver';

export class Observable<T> implements IObservable<T> {
    observers: IObserver[] = [];
    value!: T;

    addObserver(observer: IObserver): void {
        if (typeof observer.update === 'function') {
            this.observers.push(observer);
        }
    }

    removeObserver(observer: IObserver): void {
        this.observers = this.observers.filter(item => item !== observer);
    }

    removeObservers(): void {
        this.observers = [];
    }

    countObservers(): number {
        return this.observers.length;
    }

    notifyObservers(payload: T):void {
        this.value = payload;

        for (let i = this.observers.length; i--;) {
			let observer: IObserver = this.observers[i];
            observer.update(this);
		}
    }

    getValue(): T {
        return this.value;
    }
}
