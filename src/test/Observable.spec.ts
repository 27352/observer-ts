import {IObserver} from '../ts/IObserver';
import {Observable} from '../ts/Observable';

describe('Observable Tests', () => {
    class DataObserver implements IObserver {
        update(observable: Observable<number>) {
            observable.getValue();
        }
    }

    let dataEvent: Observable<number>;
    let dataObserver: DataObserver;

    beforeAll(() => {
        dataEvent = new Observable();
        dataObserver = new DataObserver;

        spyOn(dataObserver, 'update').and.callThrough();
        spyOn(dataEvent, 'getValue').and.callThrough();
    });

    it('addObserver should register the observer', () => {
        const count = dataEvent.countObservers();
        dataEvent.addObserver(dataObserver);
        expect(dataEvent.countObservers()).toEqual(count + 1);
    });

    it('removeObserver should delete the observer', () => {
        const count = dataEvent.countObservers();
        dataEvent.removeObserver(dataObserver);
        expect(dataEvent.countObservers()).toEqual(count - 1);
    });

    it('removeObservers should delete all observers', () => {
        dataEvent.addObserver(new DataObserver());
        dataEvent.addObserver(new DataObserver());
        dataEvent.addObserver(new DataObserver());

        dataEvent.removeObservers();
        expect(dataEvent.countObservers()).toEqual(0);
    });

    it('countObservers should return the observers length', () => {
        expect(dataEvent.countObservers()).toEqual(dataEvent.observers.length);
    });

    it('notifyObservers should execute observers update method', () => {
        dataEvent.addObserver(dataObserver);
        dataEvent.notifyObservers(5);

        expect(dataObserver.update).toHaveBeenCalledWith(dataEvent);
    });

    it('notifyObservers should cause the Observable.getValue method to be invoked', () => {
        dataEvent.addObserver(dataObserver);
        dataEvent.notifyObservers(5);

        expect(dataEvent.getValue).toHaveBeenCalled();
    });
});
