import { IObservable } from './IObservable';
import { IObserver } from './IObserver';
export declare class Observable<T> implements IObservable<T> {
    observers: IObserver[];
    value: T;
    addObserver(observer: IObserver): void;
    removeObserver(observer: IObserver): void;
    removeObservers(): void;
    countObservers(): number;
    notifyObservers(payload: T): void;
    getValue(): T;
}
