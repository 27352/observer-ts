"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Observable {
    constructor() {
        this.observers = [];
    }
    addObserver(observer) {
        if (typeof observer.update === 'function') {
            this.observers.push(observer);
        }
    }
    removeObserver(observer) {
        this.observers = this.observers.filter(item => item !== observer);
    }
    removeObservers() {
        this.observers = [];
    }
    countObservers() {
        return this.observers.length;
    }
    notifyObservers(payload) {
        this.value = payload;
        for (let i = this.observers.length; i--;) {
            let observer = this.observers[i];
            observer.update(this);
        }
    }
    getValue() {
        return this.value;
    }
}
exports.Observable = Observable;
//# sourceMappingURL=Observable.js.map