export interface IObservable<T> {
    getValue(): T;
}
